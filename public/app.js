var wordBank = ['apple', 'orange', 'banana'];
var randomWordSelected = '';
var lettersInRandomWordSelected = [];
var numOfBlanks = 0;
var correctAndBlanks = [];
var incorrectGuesses = []
var winCounter = 0;
var lossCounter = 0;
var numGuessesLeft = 9;

var startGame = () => {

  numGuessesLeft = 9;

  randomWordSelected = wordBank[Math.floor(Math.random() * wordBank.length)];

  lettersInRandomWordSelected = randomWordSelected.split('');
  
  numOfBlanks = lettersInRandomWordSelected.length;

  //reset for each round
  correctAndBlanks = [];
  incorrectGuesses = [];

  for (var i = 0; i < numOfBlanks; i++) {
    correctAndBlanks.push("_");
  }

  document.getElementById("startGame").innerHTML = "";
  document.getElementById("startGame").innerHTML = "<h1>You are now playing hangman</h1><p class='instruction'><span class='ins'>Press any key to select a letter</span><span class='ins'>Guess the word to win!!</span></p>";
  document.getElementById("guesses_left").innerHTML = "Number of guesses left: " + numGuessesLeft;
  document.getElementById("word_blank").innerHTML = correctAndBlanks.join(" ");
  document.getElementById("incorrect_guesses").innerHTML = incorrectGuesses.join(" ");


  document.onkeyup = function(event) {
    var letterGuessed = String.fromCharCode(event.which).toLowerCase();
    checkLetters(letterGuessed);
    endOfRound();
  }
}


function checkLetters(letter) {
  var isLetterInWord = false; 
  
  for (var i = 0; i < numOfBlanks; i++) {
    if (randomWordSelected[i] === letter) {
      isLetterInWord = true;
    }
  }

  if (isLetterInWord) {
    for (var i = 0; i < numOfBlanks; i++) {
      if(randomWordSelected[i] === letter) {
        correctAndBlanks[i] = letter;
      }
    }
  } else {
    incorrectGuesses.push(letter);
    numGuessesLeft -= 1;
  }
}

function endOfRound() {
  document.getElementById("guesses_left").innerHTML = "Number of guesses left: " +  numGuessesLeft;
  document.getElementById("word_blank").innerHTML = correctAndBlanks.join(" ");
  document.getElementById("incorrect_guesses").innerHTML = incorrectGuesses.join(" ");

if (lettersInRandomWordSelected.toString() === correctAndBlanks.toString()) {

  winCounter += 1;
  alert("We Have a Winner");
  document.getElementById("win_counter").innerHTML = winCounter;
  startGame();

} else if (numGuessesLeft === 0) {
  lossCounter += 1;
  alert("Sorry Try Again!!");
  document.getElementById("loss_counter").innerHTML = "Number of losses: " + lossCounter;
  startGame();
 }
}

document.onkeyup = function(event) {
  var letterGuessed = String.fromCharCode(event.which).toLowerCase();
  checkLetters(letterGuessed);
  endOfRound();
}

