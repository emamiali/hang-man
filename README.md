######Welcome To Hangman

In this app I used plain Javascript to build a simple hangman game. Then used CSS to add very minimal styling.

You can play the game on Heorku (Here)[https://frozen-caverns-86656.herokuapp.com/]
or you can clone this repo and run locally.
To Run locally on your machine
1. Clone this Repo
2. `npm install`
3. Navigate to localhost:5000
4. Enjoy the game




Answers are: apple, orange, banana