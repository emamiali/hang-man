const express = require('express');

const app =  express();

//server static files
app.use(express.static('public'));

app.get('/', (req, res) => {
  res.json({ Message: 'Hello World!!!' });
});


//PORT setup for dev env
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log('app listening on port: ', PORT);
});
